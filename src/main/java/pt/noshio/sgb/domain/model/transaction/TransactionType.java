/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.domain.model.transaction;

import javafx.scene.input.TransferMode;

/**
 *
 * @author martins
 */
public enum TransactionType {
    DEPOSIT,
    WITHDRAW,
    TRANSFER;
}
