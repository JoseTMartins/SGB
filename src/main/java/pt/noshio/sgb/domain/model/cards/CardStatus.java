/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.domain.model.cards;

import org.omg.PortableInterceptor.ACTIVE;

/**
 *
 * @author martins
 */
public enum CardStatus {
    ACTIVE,
    SUSPENDED,
    VOIDED;    
}
