package pt.noshio.sgb.domain.model.transaction;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import pt.noshio.sgb.domain.generators.SGBIDGenerator;
import pt.noshio.sgb.domain.interfaces.Account;
import pt.noshio.sgb.domain.interfaces.TransactionObserver;
import pt.noshio.sgb.domain.model.clients.Client;
import pt.noshio.sgb.services.AccountService;

/**
 *
 * @author martins
 */
@Data
public class Transaction {

    @Setter(AccessLevel.PRIVATE)
    private UUID transactionID;//This setter has private access only
    private Account originAccount;
    private Account destinationAccount;
    private Client client;
    @Setter(AccessLevel.NONE)
    private final Instant time;
    private BigDecimal amount;
    private TransactionType type;
    private TransactionStatus status;
    private static final  List<TransactionObserver> observers;

    static {
        observers = new ArrayList<>();
        //add below services that will observe this transaction
        addObserver(new AccountService());
   }

    /**
     * this object holds a transaction.
     *
     * SpecialNote : In a Deposit, accountOriginID and AccountDestinationID will
     * be the same.
     */
    public Transaction() {
        this.transactionID = SGBIDGenerator.ID.generate();
        this.time = Instant.now();
    }

    /**
     * When committed all transactionObservers will
     * be notified
     */
    public void commit() {
        notifyObservers();
    }

    private static void addObserver(TransactionObserver obs) {
        observers.add(obs);
    }

    public void removeObserver(TransactionObserver obs) {
        observers.remove(obs);
    }

    private void notifyObservers() {
        observers.forEach((obs) -> {
            obs.processTransaction(this);
        });
    }

}
