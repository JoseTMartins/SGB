package pt.noshio.sgb.domain.model.accounts;

import java.math.BigDecimal;
import pt.noshio.sgb.domain.interfaces.Account;

/**
 * IAccount creation provider
 *
 * @author martins
 */
public class AccountFactory {

    /**
     * Constructs & supply a new IAccount of the requested type. The account will
     * be supplied with an account ID and a balance amount of zero.
     *
     * @param type
     * @return IAccount
     */
    public static Account constructNew(AccountType type) {

        switch (type) {
            case CHECKING:
                return new CheckingAccount(BigDecimal.ZERO);

            case SAVINGS:
                return new SavingsAccount(BigDecimal.ZERO);

            case INVESTIMENT:
                return new InvestimentAccount(BigDecimal.ZERO);

            case DEPOSITCERTIFICATE:
                return new DepositCertificate(BigDecimal.ZERO);

        }
        throw new NullPointerException("Account Type cant be null");
        // TODO Study how to return an Optional

    }

}
