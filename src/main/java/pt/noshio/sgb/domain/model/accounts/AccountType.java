package pt.noshio.sgb.domain.model.accounts;

import java.util.Objects;

/**
 *
 * @author martins
 */
public enum AccountType {
    CHECKING(0),
    SAVINGS(1),
    INVESTIMENT(2),
    DEPOSITCERTIFICATE(3);

    private Integer type;

    AccountType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public static String getEnumByValue(Integer value) {
        for (AccountType e : AccountType.values()) {
            if (Objects.equals(value, e.type)) {
                return e.name();
            }
        }
        return null;//remember to check in caller
    }
}
