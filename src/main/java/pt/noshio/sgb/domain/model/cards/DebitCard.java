package pt.noshio.sgb.domain.model.cards;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import pt.noshio.sgb.domain.generators.SGBIDGenerator;
import pt.noshio.sgb.domain.interfaces.Account;
import pt.noshio.sgb.domain.interfaces.Card;

/**
 * simple model pojo
 *
 * @author martins
 */
@Data
@AllArgsConstructor
class DebitCard implements Card {

    private UUID cardNumber;
    private CardType type;
    private BigDecimal plafond;
    private CardStatus status;
    private Account linkedAccount;

    public DebitCard() {
        this.type = CardType.DEBIT;
    }

    public DebitCard(BigDecimal plafond, Account linkAccount) {
        this();
        this.cardNumber = SGBIDGenerator.ID.generate();
        this.plafond = BigDecimal.ZERO; // we leve this here. Maybe latter we can give debitcards some plafond
        this.status = CardStatus.ACTIVE;
        this.linkedAccount = linkAccount;
    }

    @Override
    public UUID getCardID() {
        return this.cardNumber;
    }

    @Override
    public BigDecimal getCardPlafond() {
        return this.plafond;
    }

    @Override
    public CardType getCardTypeType() {
        return this.type;
    }

    @Override
    public Account getLinkedAccount() {
        return this.linkedAccount;
    }

}
