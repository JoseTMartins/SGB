/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.domain.model.cards;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import pt.noshio.sgb.domain.generators.SGBIDGenerator;
import pt.noshio.sgb.domain.interfaces.Account;
import pt.noshio.sgb.domain.interfaces.Card;

/**
 * simple model pojo
 *
 * @author martins
 */
@Data
@AllArgsConstructor
class CreditCard implements Card {

    private UUID cardNumber;
    private CardType type;
    private BigDecimal plafond;
    private CardStatus status;
    private Account linkedAccount;

    public CreditCard() {
        this.type = CardType.CREDIT;
    }

    public CreditCard(BigDecimal plafond, Account linkedAccount) {
        this();
        this.cardNumber = SGBIDGenerator.ID.generate();
        this.plafond = plafond;
        this.status=CardStatus.ACTIVE;
        this.linkedAccount=linkedAccount;
    }

    @Override
    public UUID getCardID() {
        return this.cardNumber;
    }

    @Override
    public BigDecimal getCardPlafond() {
        return this.plafond;
    }

    @Override
    public CardType getCardTypeType() {
        return this.type;
    }
    
    @Override
    public Account getLinkedAccount(){
        return this.linkedAccount;
    }
    
   

}
