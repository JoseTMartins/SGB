package pt.noshio.sgb.domain.model.cards;

import java.math.BigDecimal;
import java.util.UUID;
import pt.noshio.sgb.domain.generators.SGBIDGenerator;
import pt.noshio.sgb.domain.interfaces.Account;
import pt.noshio.sgb.domain.interfaces.Card;

/**
 * Generates a new ICard of the requested type
 with requested plafond.
 * Constraint { ALL DEBIT CARDS WILL HAVE A ZERO PLAFOND }
 * @author martins
 */
public class CardFactory {

    /**
     * Constructs & supply a new ICard of the requested type.
     * The card will be supplied with a CardId.
     * Debit cards will always have a plafond of Zero, no matter
     * what value is sent as parameter.
     * @param type
     * @param plafond
     * @return ICard
     */
    public static Card constructNew(CardType type, BigDecimal plafond,Account linkedAccount) {
        if (type == null) {
            throw new NullPointerException("Card Type cant be null");
            //TODO :we could default to checking account as a default...check later
        }
        
        switch (type){
            
            case DEBIT:
                return new DebitCard(BigDecimal.ZERO,linkedAccount); //No planfond accepted for a debt card
            
            case CREDIT:
                return new CreditCard(plafond,linkedAccount);
        }
        throw new NullPointerException("Card Type cant be null");
    }
    
}
