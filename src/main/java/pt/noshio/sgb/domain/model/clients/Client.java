/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.domain.model.clients;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pt.noshio.sgb.domain.interfaces.Account;
import pt.noshio.sgb.domain.interfaces.Card;

/**
 *
 * @author martins
 */
@Data
@AllArgsConstructor
public class Client {

    private UUID ID;
    private String nif;
    private String name;
    private String address;
    private String phone;
    private String email;
    private String job;
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Account> accountList = new ArrayList<>();//Not handled by Lombok
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Card> cardList = new ArrayList<>();//Not handled by Lombok
    private CustomerType type;

    public Client() {
    }

    public Account addAccount(Account account) {
        this.accountList.add(account);
        return account;
    }

    public void addCard(Card card) {
        this.cardList.add(card);
    }

    public List<Account> getAccounts() {
        return this.accountList;
    }

    public List<Card> getCards() {
        return this.cardList;
    }

}
