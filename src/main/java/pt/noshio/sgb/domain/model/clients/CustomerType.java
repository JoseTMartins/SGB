package pt.noshio.sgb.domain.model.clients;

import java.util.Objects;

/**
 *
 * @author martins
 */
public enum CustomerType {
    STANDARD(0),
    VIP(1);

    private Integer type;

    CustomerType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public static String getEnumByValue(Integer value) {
        for (CustomerType e : CustomerType.values()) {
            if (Objects.equals(value, e.type)) {
                return e.name();
            }
        }
        return null; //remember to check in caller
        //TODO STUDY how OPTIONAL can help
    }
}
