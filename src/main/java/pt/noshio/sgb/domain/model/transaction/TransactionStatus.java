/*
 * (c) 2017 JMartins
 * Sep 27, 2017 / 2:44:26 PM
 */
package pt.noshio.sgb.domain.model.transaction;

import com.sun.corba.se.impl.activation.ProcessMonitorThread;

/**
 *
 * @author martins
 */
public enum TransactionStatus {
    PROCESSED,
    FAILED;
}
