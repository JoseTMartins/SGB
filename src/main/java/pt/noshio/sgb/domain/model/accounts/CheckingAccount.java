package pt.noshio.sgb.domain.model.accounts;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import pt.noshio.sgb.domain.generators.SGBIDGenerator;
import pt.noshio.sgb.domain.interfaces.Account;

@Data
@AllArgsConstructor
class CheckingAccount implements Account {

    private UUID accountID;
    private AccountType type;
    private BigDecimal balance;
    
    public CheckingAccount() {
        this.type = AccountType.CHECKING;//Just to avoid future problems.
    }

    public CheckingAccount(BigDecimal balance) {
        this();
        this.accountID = SGBIDGenerator.ID.generate();
        this.balance = balance;
    }

    @Override
    public UUID getAccountID() {
        return this.accountID;
    }

    @Override
    public BigDecimal getAccountBalance() {
        return this.balance;
    }

    @Override
    public AccountType getAccountType() {
        return this.type;
    }
    
    @Override
    public BigDecimal creditAccount(BigDecimal amount){
        this.balance=balance.add(amount);
        return this.balance;
    }

    @Override
    public BigDecimal debitAccount(BigDecimal amount) {
       this.balance=balance.subtract(balance);
       return this.balance;
    }
    
 


}
