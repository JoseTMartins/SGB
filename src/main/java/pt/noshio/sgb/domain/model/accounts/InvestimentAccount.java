package pt.noshio.sgb.domain.model.accounts;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import pt.noshio.sgb.domain.generators.SGBIDGenerator;
import pt.noshio.sgb.domain.interfaces.Account;

/**
 *
 * @author martins
 */
@Data
@AllArgsConstructor
class InvestimentAccount implements Account {

    private UUID accountID;
    private AccountType type;
    private BigDecimal balance;

    public InvestimentAccount() {
        this.type = AccountType.INVESTIMENT; //Just to avoid future problems.
    }

    public InvestimentAccount(BigDecimal balance) {
        this();
        this.accountID = SGBIDGenerator.ID.generate();
        this.balance = balance;
    }

    @Override
    public UUID getAccountID() {
        return this.accountID;
    }

    @Override
    public BigDecimal getAccountBalance() {
        return this.balance;
    }

    @Override
    public AccountType getAccountType() {
        return this.type;
    }

    @Override
    public BigDecimal creditAccount(BigDecimal amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BigDecimal debitAccount(BigDecimal amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
