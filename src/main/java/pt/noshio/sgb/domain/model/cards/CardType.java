package pt.noshio.sgb.domain.model.cards;

import java.util.Objects;

/**
 *
 * @author martins
 */
public enum CardType {
    CREDIT(0),
    DEBIT(1);
    
    private Integer type;

    CardType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public static String getEnumByValue(Integer value) {
        for (CardType e : CardType.values()) {
            if (Objects.equals(value, e.type)) {
                return e.name();
            }
        }
        return null;//remember to check in caller
    }
    
}
