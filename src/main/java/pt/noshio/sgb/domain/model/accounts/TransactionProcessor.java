package pt.noshio.sgb.domain.model.accounts;

import java.math.BigDecimal;
import pt.noshio.sgb.domain.interfaces.Account;
import pt.noshio.sgb.domain.model.transaction.Transaction;
import pt.noshio.sgb.domain.model.transaction.TransactionType;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author martins
 */
public class TransactionProcessor {
    
    
    public boolean ProcessTransaction(Transaction transactionToProcess){
        
        TransactionType type = transactionToProcess.getType();
        switch (type) {

            case DEPOSIT:
                return DepositProcessor(transactionToProcess);

            case WITHDRAW:
                return WithdrawProcessor(transactionToProcess);
                
            case TRANSFER:
                break;
        }
        
        return false;
    }
    
    

    public boolean DepositProcessor(Transaction transaction) {
        System.out.println("Processing Deposit");
        BigDecimal amountToDeposit = transaction.getAmount();
        Account accountToCredit = transaction.getDestinationAccount();
        if (accountToCredit.getAccountType() == AccountType.CHECKING) {
            accountToCredit.creditAccount(amountToDeposit);
            return true;
        } else {
            return false; //Not a checking account. Withdraws not allowed
        }
    }

    public boolean WithdrawProcessor(Transaction transaction) {
        System.out.println("Processing Withdraw");
        BigDecimal amountToWithdraw = transaction.getAmount();
        Account accountToDebit = transaction.getDestinationAccount();
        BigDecimal currentBalance = accountToDebit.getAccountBalance();
        if (accountToDebit.getAccountType() == AccountType.CHECKING) {
            int authorization = currentBalance.compareTo(amountToWithdraw);//Do we have enough money in Account ?
            if (authorization == 0 || authorization == 1) {
                accountToDebit.debitAccount(amountToWithdraw);
                return true;//we have enough money
            } else {
                return false;//money is not enough.
            }
        }
        return false;//Not a checking account. Withdraws not allowed
    }
    
    public boolean transferProcessor(Transaction transaction){
        throw new NotImplementedException();
    } 

}
