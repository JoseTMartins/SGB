/*
 * (c) 2017 JMartins
 * Sep 27, 2017 / 2:14:49 PM
 */
package pt.noshio.sgb.domain.exceptions;

/**
 *
 * @author martins
 */
public class TransactionException extends Exception {
    
    public TransactionException(){}
    
    public TransactionException(String errorMessage){
        super(errorMessage);
    }
    
}
