
package pt.noshio.sgb.domain.generators;

import java.util.UUID;

/**
 *
 * @author martins
 */


public enum SGBIDGenerator {

    /**
     * Generates a Unique Identifier for a SGB Unit
     * Client,Account,or Card
     */
    ID;
        public UUID  generate() {
        return UUID.randomUUID();
    }
}
