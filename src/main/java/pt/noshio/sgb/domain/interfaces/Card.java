package pt.noshio.sgb.domain.interfaces;

import java.math.BigDecimal;
import java.util.UUID;
import pt.noshio.sgb.domain.model.cards.CardType;

/**
 *
 * @author martins
 */
public interface Card {

    
    UUID getCardID();
    
    BigDecimal getCardPlafond();
    
    CardType getCardTypeType();
    
    Account getLinkedAccount();
    
}
