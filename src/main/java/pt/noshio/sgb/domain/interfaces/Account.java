package pt.noshio.sgb.domain.interfaces;

import java.math.BigDecimal;
import java.util.UUID;
import pt.noshio.sgb.domain.model.accounts.AccountType;

/**
 *
 * @author martins
 */

public interface Account {
   
    UUID getAccountID();
    
    BigDecimal getAccountBalance();
    
    AccountType getAccountType();
    
    BigDecimal creditAccount(BigDecimal amount);
    
    BigDecimal debitAccount(BigDecimal amount);
    
}
