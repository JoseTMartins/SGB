package pt.noshio.sgb.domain.interfaces;

import pt.noshio.sgb.domain.model.transaction.Transaction;

/**
 *
 * @author martins
 */
public interface TransactionObserver {
    
  public boolean processTransaction(Transaction transaction);
  
}
