package pt.noshio.sgb.services;

import java.math.BigDecimal;
import pt.noshio.sgb.domain.model.accounts.AccountType;
import pt.noshio.sgb.domain.model.cards.CardType;
import pt.noshio.sgb.domain.model.accounts.AccountFactory;
import pt.noshio.sgb.domain.model.cards.CardFactory;
import pt.noshio.sgb.domain.generators.SGBIDGenerator;
import pt.noshio.sgb.domain.model.clients.Client;
import pt.noshio.sgb.persistence.ClientPersistence;
import pt.noshio.sgb.domain.model.transaction.Transaction;
import pt.noshio.sgb.domain.model.transaction.TransactionType;
import pt.noshio.sgb.domain.interfaces.Account;

/**
 * Handles all the processes related to customers
 *
 * @author martins
 */
public class ClientService {

    /**
     * Creates a new Client & Sets up a checking account with a balance of Zero,
     * creates a DebitCard and associates the DebitCard with the account
     *
     * @param nif
     * @param name
     * @param address
     * @param phone
     * @param job
     * @return Client
     */
    public Client createNewClient(String nif, String name, String address, String phone, String job) {
        Client client = new Client();
        client.setID(SGBIDGenerator.ID.generate());
        client.setNif(nif);
        client.setName(name);
        client.setAddress(address);
        client.setPhone(phone);
        client.setJob(job);
        setupNewAccount(client);
        return persist(client);
    }

    private Client persist(Client client) {
        return ClientPersistence.addCLient(client);
    }

    private void setupNewAccount(Client client) {
        Account newAccount = client.addAccount(AccountFactory.constructNew(AccountType.CHECKING));
        client.addCard(CardFactory.constructNew(CardType.DEBIT, BigDecimal.ZERO, newAccount)); //Linking card->Account happens here
    }

    /**
     * Creates a Transaction and commits that transaction to the
     * transactionQueue to be processed by the account service.
     *
     * @param client
     * @param account
     * @param amount
     * @return
     */
    public boolean checkingAccountTransaction(Client client, Account account, TransactionType transactionType, BigDecimal amount) {
        //check if account is a checking account
        if (account.getAccountType() == AccountType.CHECKING) {
            Transaction transaction = new Transaction();
            transaction.setOriginAccount(account);
            transaction.setDestinationAccount(account);
            transaction.setType(transactionType);
            transaction.setAmount(amount);
            transaction.setClient(client);
            transaction.commit();
        }
        throw new Error("Conta não aceita depositos" + account.getAccountType().toString()); // handle this exceptions later.
    }



}
