package pt.noshio.sgb.services;

import pt.noshio.sgb.domain.interfaces.TransactionObserver;
import pt.noshio.sgb.domain.model.accounts.TransactionProcessor;
import pt.noshio.sgb.domain.model.transaction.Transaction;

/**
 * The AccountService is an automatic service There should be no direct calls to
 * this class from front end.
 *
 * @author martins
 */
public class AccountService implements TransactionObserver {

    public AccountService() {
    }

    @Override
    public boolean processTransaction(Transaction transactionToProcess) {
        //we are going to get the transaction
        System.out.println("PROCESSING TRANSACTION");
        boolean isTransactionSuccessfull=new TransactionProcessor().ProcessTransaction(transactionToProcess);
        return isTransactionSuccessfull;//could be one line...but....not easy to understand.
    }

}
