/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.persistence;

import java.util.ArrayList;
import java.util.List;
import pt.noshio.sgb.domain.model.transaction.Transaction;

/**
 *
 * @author martins
 */
public enum TransactionPersistence {
    
    INSTANCE;
    private static final List<Transaction> transactionBag = new ArrayList<>();

    public static Transaction addTransaction(Transaction transaction) {
        transactionBag.add(transaction);
        return transaction;
    }

    public static Transaction getTransactionAt(int index) {
        return transactionBag.get(index);
    }

    public static List<Transaction> clearAll() {
        transactionBag.clear();
        return transactionBag;
    }
    
}
