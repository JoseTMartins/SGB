package pt.noshio.sgb.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import pt.noshio.sgb.domain.model.clients.Client;

/**
 * Singleton for client persistence
 *
 * @author martins
 */
public enum ClientPersistence {

    INSTANCE;

    private static final List<Client> clientBag = new ArrayList<>();

    public static Client addCLient(Client c) {
        clientBag.add(c);
        return c;
    }

    public List<Client> getAllClients() {
        return clientBag;
    }

    public void clearAllClients() {
        clientBag.clear();
    }

    public Client getClientAtIndex(int index) {
        return clientBag.get(index);
    }

    public Optional<Client> getClientByNIF(String nif) {
//----------------------------------------------------------
//      only returns a boolean. Not enough for future
//----------------------------------------------------------
//        clientBag.forEach(customer -> clientBag.stream()
//                .anyMatch(n -> n.getNif().equals(nif)));
//--------------------------------------------------------------
        Optional<Client> client;

        client = clientBag.stream()
                .filter(n -> n.getNif().equals(nif))
                .findFirst(); //find first returns an optional

        return client;
    }

    //----------------------------------------------------------
//      INITIAL VERSION FOR TESTING (TO DELETE)
//----------------------------------------------------------
//            for (Client customer : clientBag){
//                if (customer.getNif().equals(nif)){
//                    return customer;
//                }          
//        }
//            return null;
}


