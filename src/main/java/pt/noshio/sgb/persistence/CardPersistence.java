package pt.noshio.sgb.persistence;


import java.util.ArrayList;
import java.util.List;
import pt.noshio.sgb.domain.interfaces.Card;

/**
 * Singleton for card persistence
 * @author martins
 */
public enum CardPersistence {
    
    INSTANCE;
    private static final List<Card> cardBag = new ArrayList<>();

    public static void addcard(Card card) {
        cardBag.add(card);
    }
    
   public static Card getAccountAt(int index) {
        return cardBag.get(index);
    }

    public static List<Card> clearAll() {
        cardBag.clear();
        return cardBag;
    }

    
}
