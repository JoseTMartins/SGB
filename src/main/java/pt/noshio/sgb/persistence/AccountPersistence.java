package pt.noshio.sgb.persistence;

import java.util.ArrayList;
import java.util.List;
import pt.noshio.sgb.domain.interfaces.Account;

/**
 * Singleton for IAccount persistence
 * @author martins
 */
public enum AccountPersistence {

    INSTANCE;
    private static final List<Account> accountBag = new ArrayList<>();

    public static void addAccount(Account account) {
        accountBag.add(account);
    }

    public static Account getAccountAt(int index) {
        return accountBag.get(index);
    }

    public static List<Account> clearAll() {
        accountBag.clear();
        return accountBag;
    }
}
