/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.domain.model.accounts;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pt.noshio.sgb.domain.interfaces.Account;

/**
 *
 * @author martins
 */
public class AccountFactoryTest {

    static Account savingsAccount;
    static Account investmentAccount;
    static Account checkingAccount;
    static Account depositCertificateAccount;

    public AccountFactoryTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        savingsAccount = AccountFactory.constructNew(AccountType.SAVINGS);
        investmentAccount = AccountFactory.constructNew(AccountType.INVESTIMENT);
        checkingAccount = AccountFactory.constructNew(AccountType.CHECKING);
        depositCertificateAccount = AccountFactory.constructNew(AccountType.DEPOSITCERTIFICATE);

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testAccountsAreOfCorrectType() {
        assertEquals(AccountType.SAVINGS, savingsAccount.getAccountType());
        assertEquals(AccountType.INVESTIMENT, investmentAccount.getAccountType());
        assertEquals(AccountType.CHECKING, checkingAccount.getAccountType());
        assertEquals(AccountType.DEPOSITCERTIFICATE, depositCertificateAccount.getAccountType());
    }

    @Test
    public void testAccountBalancesisZero() {
        assertEquals(BigDecimal.ZERO, savingsAccount.getAccountBalance());
        assertEquals(BigDecimal.ZERO, investmentAccount.getAccountBalance());
        assertEquals(BigDecimal.ZERO, checkingAccount.getAccountBalance());
        assertEquals(BigDecimal.ZERO, depositCertificateAccount.getAccountBalance());
    }

    @Test
    public void testAllAccountNumbersAreDifferent() {
        //Set will not accept duplicate values.
        //So in the end size will have to be the same
        //as the number off accounts.
        Set<UUID> accountnumbersSet = new HashSet<>();
        accountnumbersSet.add(savingsAccount.getAccountID());
        accountnumbersSet.add(investmentAccount.getAccountID());
        accountnumbersSet.add(checkingAccount.getAccountID());
        accountnumbersSet.add(depositCertificateAccount.getAccountID());
        assertEquals(4, accountnumbersSet.size());

    }

}
