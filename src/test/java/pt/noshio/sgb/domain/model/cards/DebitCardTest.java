/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.domain.model.cards;

import java.math.BigDecimal;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pt.noshio.sgb.domain.model.accounts.AccountFactory;
import pt.noshio.sgb.domain.model.accounts.AccountType;
import static pt.noshio.sgb.domain.model.cards.CreditCardTest.account1;
import pt.noshio.sgb.domain.interfaces.Account;

/**
 *
 * @author martins
 */
public class DebitCardTest {

    static Account account1;
    static Account account2;

    static DebitCard card1;
    static DebitCard card2;

    public DebitCardTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        account1 = AccountFactory.constructNew(AccountType.CHECKING);
        account2 = AccountFactory.constructNew(AccountType.CHECKING);
        card1 = new DebitCard(BigDecimal.TEN, account1);
        card2 = new DebitCard(BigDecimal.valueOf(500),account2);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testCardIsCreatedProperly() {
        assertNotNull(card1);
        assertNotNull(card2);
    }

    @Test
    public void testCardsHaveID() {
        assertNotNull(card1.getCardID());
        assertNotNull(card2.getCardID());
    }

    @Test
    public void testCardsAreOfProperType() {
        assertEquals(CardType.DEBIT, card1.getCardTypeType());
        assertEquals(CardType.DEBIT, card2.getCardTypeType());
    }

    @Test
    public void testPlafondisZeroforDebitCard() {
        assertEquals(BigDecimal.ZERO, card1.getPlafond());
        assertEquals(BigDecimal.ZERO, card2.getPlafond());
    }

    @Test
    public void testCardsIDAreDifferent() {
        assertNotEquals(card1.getCardID(), card2.getCardID());
    }
    
    @Test
    public void testCardsHaveAnAccount(){
        assertEquals(account1, card1.getLinkedAccount());
        assertEquals(account2, card2.getLinkedAccount());
    }
            

}
