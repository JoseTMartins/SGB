/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.domain.model.accounts;

import java.math.BigDecimal;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author martins
 */
public class SavingsAccountTest {

    static SavingsAccount account1;
    static SavingsAccount account2;

    public SavingsAccountTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        account1 = new SavingsAccount(BigDecimal.ZERO);
        account2 = new SavingsAccount(BigDecimal.TEN); // This is to try to fool the system
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testAccountIsCreatedProperly() {
        assertNotNull(account1);
        assertNotNull(account2);
    }

    @Test
    public void testAccountsHaveID() {
        assertNotNull(account1.getAccountID());
        assertNotNull(account2.getAccountID());
    }

    @Test
    public void testAccountsAreOfProperType() {
        assertEquals(AccountType.SAVINGS, account1.getAccountType());
        assertEquals(AccountType.SAVINGS, account2.getAccountType());
    }

    @Test
    public void testBalanceisZeroforANewAccount() {
        assertEquals(BigDecimal.ZERO, account1.getBalance());
        assertEquals(BigDecimal.TEN, account2.getBalance());
    }

    @Test
    public void testAccountsIDAreDifferent() {
        assertNotEquals(account1.getAccountID(), account2.getAccountID());

    }

}
