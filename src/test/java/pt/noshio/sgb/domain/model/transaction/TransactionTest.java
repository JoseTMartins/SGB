package pt.noshio.sgb.domain.model.transaction;

import java.math.BigDecimal;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pt.noshio.sgb.domain.model.clients.Client;
import pt.noshio.sgb.persistence.AccountPersistence;
import pt.noshio.sgb.persistence.ClientPersistence;
import pt.noshio.sgb.persistence.TransactionPersistence;
import pt.noshio.sgb.services.ClientService;

/**
 *
 * @author martins
 */
public class TransactionTest {

    static Transaction transaction1 = null;
    static Transaction transaction2 = null;
    static Client customer1 = null;
    static Client customer2 = null;

    public TransactionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        TransactionPersistence.INSTANCE.clearAll();
        AccountPersistence.INSTANCE.clearAll();
        ClientPersistence.INSTANCE.clearAllClients();
        //create clients
        customer1 = new ClientService().createNewClient("123456789", "Jose Luis Afonso", "Rua dos Afonsinhos", "111111111", "Job1");
        customer2 = new ClientService().createNewClient("987654321", "Ricardo Manuel dos Reis", "Rua da realeza", "222222222", "Job2");
        transaction1 = new Transaction();
        transaction2 = new Transaction();

    }

    @AfterClass
    public static void tearDownClass() {
        AccountPersistence.INSTANCE.clearAll();
        ClientPersistence.INSTANCE.clearAllClients();
        TransactionPersistence.INSTANCE.clearAll();
    }

    @Test
    public void testTransactions() {
        transaction1.setClient(customer1);
        transaction1.setOriginAccount(customer1.getAccounts().get(0));
        transaction1.setDestinationAccount(customer1.getAccounts().get(0));
        transaction1.setType(TransactionType.DEPOSIT);
        transaction1.setAmount(BigDecimal.valueOf(2350));
        System.out.println("(TEST) TRANSACTION COMIT1");
        transaction1.commit();
        assertEquals(BigDecimal.valueOf(2350), customer1.getAccounts().get(0).getAccountBalance());

        transaction2.setClient(customer2);
        transaction2.setOriginAccount(customer2.getAccounts().get(0));
        transaction2.setDestinationAccount(customer2.getAccounts().get(0));
        transaction2.setType(TransactionType.DEPOSIT);
        transaction2.setAmount(BigDecimal.valueOf(850));
        System.out.println("(TEST) TRANSACTION COMIT2");
        transaction2.commit();
        assertEquals(BigDecimal.valueOf(850), customer2.getAccounts().get(0).getAccountBalance());
        
    }

//    The testBellow fails, as accounts apear cleared..WHY ? 
//    What is clearing the Accounts ?
//    @Test
//    public void checkBalance() {
//        // Account account1 = customer1.getAccounts().get(0);
//        assertEquals(BigDecimal.valueOf(2350), customer1.getAccounts().get(0).getAccountBalance());
//        //Account account2 = customer2.getAccounts().get(0);
//        assertEquals(BigDecimal.valueOf(850), customer1.getAccounts().get(0).getAccountBalance());
//
//    }

}
