/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.noshio.sgb.domain.model.accounts;

import java.math.BigDecimal;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author martins
 */
public class DepositCertificateTest {

    static DepositCertificate account1;
    static DepositCertificate account2;

    public DepositCertificateTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        account1 = new DepositCertificate(BigDecimal.ZERO);
        account2 = new DepositCertificate(BigDecimal.valueOf(500));
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testAccountIsCreatedProperly() {
        assertNotNull(account1);
        assertNotNull(account2);
    }

    @Test
    public void testAccountsHaveID() {
        assertNotNull(account1.getAccountID());
        assertNotNull(account2.getAccountID());
    }

    @Test
    public void testAccountsAreOfProperType() {
        assertEquals(AccountType.DEPOSITCERTIFICATE, account1.getAccountType());
        assertEquals(AccountType.DEPOSITCERTIFICATE, account2.getAccountType());
    }

    @Test
    public void testBalance() {
        assertEquals(BigDecimal.ZERO, account1.getAccountBalance());
        assertEquals(BigDecimal.valueOf(500), account2.getAccountBalance());
    }

    @Test
    public void testAccountsIDAreDifferent() {
        assertNotEquals(account1.getAccountID(), account2.getAccountID());

    }

}
