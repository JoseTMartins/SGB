package pt.noshio.sgb.services;

import java.math.BigDecimal;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pt.noshio.sgb.domain.model.clients.Client;
import java.util.List;
import pt.noshio.sgb.domain.model.accounts.AccountType;
import pt.noshio.sgb.domain.model.transaction.Transaction;
import pt.noshio.sgb.domain.interfaces.Account;
import pt.noshio.sgb.domain.interfaces.Card;
import pt.noshio.sgb.domain.model.transaction.TransactionType;

/**
 * This actually ends up testing account factory and 
 * card factory at the same time.
 * @author martins
 */
public class ClientServiceTest {

    static Client customer1;
    static Client customer2;

    public ClientServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {

        //This will create a new client, a new Checking account, a new ICard and persist the client.
        customer1 = new ClientService().createNewClient("123456789", "Jose Luis Afonso", "Rua dos Afonsinhos", "111111111", "Job1");
        customer2 = new ClientService().createNewClient("987654321", "Ricardo Manuel dos Reis", "Rua da realeza", "222222222", "Job2");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void testCreateNewClient() {

        //Lets fully examine basic data from customer 1
        assertEquals(customer1.getNif(), "123456789");
        assertEquals(customer1.getName(), "Jose Luis Afonso");
        assertEquals(customer1.getAddress(), "Rua dos Afonsinhos");
        assertEquals(customer1.getPhone(), "111111111");
        assertEquals(customer1.getJob(), "Job1");
        //Lets make shure customer account List is not empty
        assertNotNull(customer1.getAccounts());
        //Lets make sure customer card list is not empty
        assertNotNull(customer1.getCards());

        //Lets fully examine basic data from customer 2
        assertEquals(customer2.getNif(), "987654321");
        assertEquals(customer2.getName(), "Ricardo Manuel dos Reis");
        assertEquals(customer2.getAddress(), "Rua da realeza");
        assertEquals(customer2.getPhone(), "222222222");
        assertEquals(customer2.getJob(), "Job2");
    }

    @Test
    public void testCustomerAccountListIsNotEmpty() {
        //Lets make shure customer account List is not empty
        assertNotNull(customer2.getAccounts());
        //Lets make sure customer card list is not empty
        assertNotNull(customer2.getCards());
    }

    @Test
    public void testCustomerNumbersAreDifferent() {
        assertNotEquals(customer1.getID(), customer2.getID());
    }

    @Test
    public void testAccountNumbersAreDiferentAndOnlyOneperCustomerAndTypeCheckingAndBalnceisZero() {
        List<Account> customer1AccountList = customer1.getAccounts();
        List<Account> customer2AccountList = customer2.getAccounts();
        assertEquals(customer1AccountList.size(), 1);
        assertEquals(customer2AccountList.size(), 1);
        
        //Ensure accountIDS are different
        
        assertNotEquals(customer1AccountList.get(0).getAccountID(), customer2AccountList.get(0).getAccountID());
        
        //EnsureBothAccounts are of checking type
        
        assertEquals(customer1AccountList.get(0).getAccountType(),AccountType.CHECKING);
        assertEquals(customer2AccountList.get(0).getAccountType(),AccountType.CHECKING);
        
        //Ensure Balance is zero.
        
        assertEquals(customer1AccountList.get(0).getAccountBalance(), BigDecimal.ZERO);
        assertEquals(customer2AccountList.get(0).getAccountBalance(), BigDecimal.ZERO);
    }
    
    @Test
    public void testCustomerHasADebitCard(){
        //We only have one account and one card per customer
        //So we can use index (0)
        List<Card> cards1=customer1.getCards();
        List<Card> cards2=customer2.getCards();
        
        List<Account> customer1Accounts=customer1.getAccounts();
        List<Account> customer2Accounts=customer2.getAccounts();
        
        Account account1=customer1Accounts.get(0);
        Account account2=customer2Accounts.get(0);
        
        assertEquals(account1, cards1.get(0).getLinkedAccount());
        assertEquals(account2, cards2.get(0).getLinkedAccount());
    }

    @Test
    public void testDeposit() {
        //Balance starts with 0 from above
        ClientService clientService= new ClientService();
        boolean checkingAccountTransaction = clientService.checkingAccountTransaction(customer1, customer1.getAccounts().get(0),TransactionType.DEPOSIT, BigDecimal.valueOf(1250)); 
        Account testAccount=customer1.getAccounts().get(0);
        assertEquals(BigDecimal.valueOf(1250), testAccount.getAccountBalance());    
    }
    


}
